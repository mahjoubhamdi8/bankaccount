## Architecture Hexagonale : Qu'est-ce que c'est ?

L'architecture Hexagonale est une architecture
proposée par Alistair Cockburn en 2005 dans un [billet de blog](https://alistair.cockburn.us/hexagonal-architecture/).


Toutes les architectures ont un même objectif : __découpler__ le code métier,
celui sur lequel repose la valeur métier d'un logiciel,
du reste du code (technique, persistence, présentation, etc).

L'architecture hexa pousse cette logique de découplage jusqu'à un extrême,
au moyen de deux contraintes :

- __L'interdiction totale__ pour le code métier d'avoir la moindre __dépendance extérieures__
- L'obligation qui est faite à tout logiciel extérieur dépendant du code métier
  (le domain) de passer par des interfaces, que l'on appelera __port__,
  pour interagir avec le code métier

Au moyen de ces deux contraintes, cette architecture
nous amène plusieurs avantages :

- Un couplage nul entre le code métier et tout le reste (persistence)
- un code métier intégralement testable de manière unitaire
- une forte cohérence entre le code métier et le reste de l'application

Dans une architecture hexagonale, on distingue trois parties
dans une application :

- la partie gauche, que l'on appelle aussi user side ou driving side  
  c'est là qu'on trouvera les contrôleurs REST et tous les points d'entrées d'ume manière génerale
  du monde extérieur vers le domaine
- la partie droite, que l'on appelle aussi server-side 
  ou driven side :  
  c'est là qu'on trouvera les dépendances du domaine : base de données,et d'une manière générale tout ce qui constitue
  un point de sortie du domaine vers le monde extérieur
- enfin la partie centrale, l'hexagone à proprement parler, le domaine métier,
où réside la logique métier et la valeur d'une application. C'est cette partie
qui nous intéresse ici

# BankAccount KATA
Ce projet a été développé en spring boot avec l'architecture hexagonale.
ce projet est composé de 4 modules principaux : 
 - Domain (domaine métier)
 - Rest Api
 - Infrastructure
 - Launcher


 ## Domain: 
 Dans le module "Domain", il y a les packages suivants : 
 - Entity : implemention des entités nécessaires dans application
 - command : un ensemble de DTO d'Action/Query qui implemente l'interface command
 - validator: permet de valider la conformité d'un objet
 - ports : les ports permettant d’appeler des fonctions dans l’hexagone et les ports permettant à l’hexagone d’appeler des éléments extérieurs.
 - adapter: l'implementation des ports permettant d’appeler des fonctions dans l’hexagone
 - mediator : présente un intermédiaire entre nos classes (ici nos UseCase), et force tous les commandes à passer par le Mediator afin d'arbitrer le choix de la classe (UseCase) à appeler.
 - UseCase : implémentation des cas d'utilisation
 - exception : aide à gérer les exceptions 

  et j'ai effectué des tests unitaires (couverture des test de ce module 96% )

  ![Hexagone](assets/Hexagone.png)

## API:
ce projet expose un API REST de quelques cas d'utilisation de l'application.
cette partie est composé de 3 packages: 
- Controller : implementation de controller 
- DTO (Data Transfer Object) : est un patron de conception utilisé pour transférer des données entre les couches d'une application. 
- mappers: utilise "MapStruct" pour simplifier le mapping (la conversion) d'objets d'un type à un autre.

### les URIs : 
#### POST "/account"  :
création d'un compte 

#### PUT "account/deposit"
Operation de dépot d'une somme {amount} dans le compte qui a l'id {id}

#### PUT "account/withdraw"
Operation de retrait d'une somme {amount} dans le compte qui a l'id {id}

#### GET "account/{id}"
Récuperer le compte qui a l'id {id}

#### GET "account/balance/{id}"
Consulter le solde actuel

#### GET "/account/history"
Consulter les transactions précédentes



## Infrastructure: 

Pour la couche de persistance, j'ai utilisé MongoDB via Spring Data.
la partie "adapter" implemente des le ports (interface) de persistance de la partie "domain".

## Launcher: 
Dans ce module, nous avons besoin d'un "starter" d'une application Spring Boot classique( une classe annoté @SpringbootApplication)
Nous avons un module "domain" qui est complètement isolé de Spring Boot, donc pour que Spring puisse instancier notre domaine et l'injecter, nous devons déclarer nos beans manuellement.

