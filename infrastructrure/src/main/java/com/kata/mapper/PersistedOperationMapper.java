package com.kata.mapper;

import com.kata.entity.Operation;
import com.kata.entity.PersistedOperation;
import org.mapstruct.Mapper;
import java.util.List;

@Mapper(componentModel = "spring")
public interface PersistedOperationMapper {
    PersistedOperation map(Operation operation);
    Operation map(PersistedOperation persistedOperation);
    List<Operation> persistedOperationsToOperationsList(List<PersistedOperation> persistedOperationList);
    List<PersistedOperation> operationsToPersistedOperationsList(List<Operation> operationList);
}
