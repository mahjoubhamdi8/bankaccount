package com.kata.mapper;

import com.kata.entity.Account;
import com.kata.entity.PersistedAccount;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {PersistedOperationMapper.class})
public interface PersistedAccountMapper {
    PersistedAccount map(Account account);
    Account map(PersistedAccount persistedAccount);
}
