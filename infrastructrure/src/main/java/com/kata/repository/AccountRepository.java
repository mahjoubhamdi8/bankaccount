package com.kata.repository;

import com.kata.entity.PersistedAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.Optional;

@Repository
public interface AccountRepository extends JpaRepository<PersistedAccount, String> {
    Optional<PersistedAccount> findByAccountNumber(String accountNumber);
}
