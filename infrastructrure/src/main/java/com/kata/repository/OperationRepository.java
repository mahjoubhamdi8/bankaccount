package com.kata.repository;

import com.kata.entity.PersistedOperation;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;
public interface OperationRepository extends JpaRepository<PersistedOperation, Long> {
    List<PersistedOperation> findByAccountNumberAndDateBetween(String accountNumber, LocalDateTime sinceDate, LocalDateTime untilDate);
}
