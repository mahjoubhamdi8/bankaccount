package com.kata.adapter;
import com.kata.entity.Account;
import com.kata.entity.PersistedAccount;
import com.kata.exception.AccountNotFoundException;
import com.kata.mapper.PersistedAccountMapper;
import com.kata.ports.persistance.AccountPersistencePort;
import com.kata.repository.AccountRepository;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import java.util.Optional;
@RequiredArgsConstructor
public class AccountPersistanceAdapter implements AccountPersistencePort {

    private final AccountRepository accountRepository;
    private final PersistedAccountMapper persistedAccountMapper ;
    @Override
    public Account createAccount(Account account) {
        PersistedAccount persistedAccount = persistedAccountMapper.map(account);
        persistedAccount = accountRepository.save(persistedAccount);
        return persistedAccountMapper.map(persistedAccount);
    }

    @Override
    public Account updateAccount(Account account) {
        return createAccount(account);
    }

    @Override
    public Account getAccountByNumber(String accountNumber) throws AccountNotFoundException {
        Optional<PersistedAccount> persistedAccount = accountRepository.findByAccountNumber(accountNumber);
        if (persistedAccount == null)
            throw new AccountNotFoundException("Account Not Found");
        return persistedAccountMapper.map(persistedAccount.get());
    }

}
