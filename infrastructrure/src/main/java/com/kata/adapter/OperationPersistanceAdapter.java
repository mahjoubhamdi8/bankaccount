package com.kata.adapter;

import com.kata.entity.Operation;
import com.kata.entity.PersistedOperation;
import com.kata.mapper.PersistedOperationMapper;
import com.kata.ports.persistance.OperationPersistancePort;
import com.kata.repository.OperationRepository;
import lombok.RequiredArgsConstructor;
import java.time.LocalDateTime;
import java.util.List;
@RequiredArgsConstructor
public class OperationPersistanceAdapter  implements OperationPersistancePort {
    private final OperationRepository operationRepository ;
    private final PersistedOperationMapper persistedOperationMapper ;

    @Override
    public Operation createOperation (Operation operation) {
        PersistedOperation persistedOperation = persistedOperationMapper.map(operation);
        persistedOperation = operationRepository.save(persistedOperation);
        return persistedOperationMapper.map(persistedOperation);
    }
    @Override
    public List<Operation> getOperationsByAccountNumberSinceDate(String accountNumber , LocalDateTime date) {
        return persistedOperationMapper.persistedOperationsToOperationsList(operationRepository.findByAccountNumberAndDateBetween(accountNumber, date, LocalDateTime.now()));
    }
}
