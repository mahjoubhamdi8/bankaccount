package com.kata.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Set;
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PersistedAccount {
    @Id
    String accountNumber;
    BigDecimal balance;
    AccountType type;
    LocalDateTime creationDate;
}
