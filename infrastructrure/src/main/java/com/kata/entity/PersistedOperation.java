package com.kata.entity;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.*;
import java.time.LocalDateTime;
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PersistedOperation {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    long id;
    LocalDateTime date;
    float amount;
    OperationType type;
    String accountNumber;
}
