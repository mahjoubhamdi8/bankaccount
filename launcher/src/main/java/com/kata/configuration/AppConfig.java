package com.kata.configuration;

import com.kata.adapter.AccountAdapter;
import com.kata.adapter.AccountPersistanceAdapter;
import com.kata.adapter.OperationPersistanceAdapter;
import com.kata.command.account.*;
import com.kata.entity.Account;
import com.kata.entity.Operation;
import com.kata.entity.PersistedOperation;
import com.kata.mapper.PersistedAccountMapper;
import com.kata.mapper.PersistedOperationMapper;
import com.kata.mediator.Mediator;
import com.kata.mediator.MediatorImpl;
import com.kata.ports.incoming.AccountServicePort;
import com.kata.ports.persistance.AccountPersistencePort;
import com.kata.ports.persistance.OperationPersistancePort;
import com.kata.repository.AccountRepository;
import com.kata.repository.OperationRepository;
import com.kata.usecase.account.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

@Configuration
public class AppConfig {

    @Bean
    public AccountPersistencePort accountPersistence(AccountRepository accountRepository , PersistedAccountMapper persistedAccountMapper) {
        return new AccountPersistanceAdapter(accountRepository , persistedAccountMapper);
    }
    @Bean
    public OperationPersistancePort operationPersistencePort(OperationRepository operationRepository , PersistedOperationMapper persistedOperationMapper) {
        return new OperationPersistanceAdapter(operationRepository , persistedOperationMapper);
    }


    @Bean
    public UseCaseAccount<Account , DepositAction> depositAmount(AccountRepository accountRepository ,OperationRepository operationRepository, PersistedAccountMapper persistedAccountMapper ,PersistedOperationMapper persistedOperationMapper ) {
        return new DepositAmountUseCase(accountPersistence(accountRepository , persistedAccountMapper) , operationPersistencePort( operationRepository, persistedOperationMapper));
    }


    @Bean
    public UseCaseAccount<Account, CreateAccountAction> createAccount(AccountRepository accountRepository , PersistedAccountMapper persistedAccountMapper) {
        return new CreateAccountUseCase(accountPersistence(accountRepository, persistedAccountMapper));
    }


    @Bean
    public UseCaseAccount<Account , GetAccountQuery> getAccount(AccountRepository accountRepository, PersistedAccountMapper persistedAccountMapper) {
        return new GetAccountUseCase(accountPersistence(accountRepository, persistedAccountMapper));
    }
    @Bean
    public UseCaseAccount<Account , WithdrawAction> withdrawAmount(AccountRepository accountRepository ,OperationRepository operationRepository ,  PersistedAccountMapper persistedAccountMapper ,PersistedOperationMapper persistedOperationMapper) {
        return new WithdrawAmountUseCase(accountPersistence(accountRepository, persistedAccountMapper), operationPersistencePort(operationRepository , persistedOperationMapper));
    }

    @Bean
    public UseCaseAccount<BigDecimal,GetBalanceQuery> getBalance(AccountRepository accountRepository, PersistedAccountMapper persistedAccountMapper ) {
        return new ConsultBalanceUseCase(accountPersistence(accountRepository, persistedAccountMapper));
    }

    @Bean
    public UseCaseAccount<List<Operation>,GetHistoryAccountQuery> getHistoryAccountQuery(OperationRepository operationRepository ,PersistedOperationMapper persistedOperationMapper) {
        return new ConsultHistoryAccountUseCase(operationPersistencePort(operationRepository, persistedOperationMapper));
    }

    @Bean
    public Mediator mediator(Set<UseCaseAccount<?,?>> usercases){
        return new MediatorImpl(usercases);
    }

    @Bean
    public AccountServicePort accountSerivce( Mediator mediator) {
        return new AccountAdapter(mediator);
    }

}
