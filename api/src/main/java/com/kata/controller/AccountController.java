package com.kata.controller;

import com.kata.command.account.*;
import com.kata.dto.AccountDto;
import com.kata.dto.OperationDto;
import com.kata.entity.Account;
import com.kata.mapper.AccountMapper;
import com.kata.mapper.OperationMapper;
import com.kata.ports.incoming.AccountServicePort;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.math.BigDecimal;
import java.util.List;

@RestController
@RequestMapping("/account")
@RequiredArgsConstructor
public class AccountController {

    private final AccountServicePort accountServicePort;
    private final AccountMapper accountMapper;
    private final OperationMapper operationMapper;

    @GetMapping(path = "/{accountNumber}" , produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AccountDto> getAccount(@PathVariable(name = "accountNumber") String accountNumber) {
        GetAccountQuery getAccountQuery = GetAccountQuery.builder().accountNumber(accountNumber).build();
        Account account = accountServicePort.getAccount(getAccountQuery);
        return ResponseEntity.ok(accountMapper.toDto(account));
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AccountDto> createAccount(@RequestBody AccountDto accountDto) {
        Account account = accountServicePort.createAccount(accountMapper.toCreateAction(accountDto));
        return ResponseEntity.status(201).body(accountMapper.toDto(account));
    }

    @PutMapping(path = "/withdraw", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity withdrawFromAccount(@RequestBody WithdrawAction withdrawAction) {
        accountServicePort.withdrawAmount(withdrawAction);
        return ResponseEntity.noContent().build();
    }

    @PutMapping(path = "/deposit" , consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity depositToAccount(@RequestBody DepositAction depositAction) {
       accountServicePort.depositAmount(depositAction);
        return ResponseEntity.noContent().build();
    }
    @GetMapping(path = "/balance/{accountNumber}" , produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Float> getBalance(@PathVariable(name = "accountNumber") String accountNumber) {
        GetBalanceQuery getBalanceQuery = GetBalanceQuery.builder().accountNumber(accountNumber).build();
        BigDecimal balance = accountServicePort.getBalance(getBalanceQuery);
        return ResponseEntity.ok(balance.floatValue());
    }

    @GetMapping(path = "/history" , produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<OperationDto>> getBalance(@RequestBody GetHistoryAccountQuery getHistoryAccountQuery) {
        List<OperationDto> dtos = operationMapper.mapToDtoList(accountServicePort.getHistory(getHistoryAccountQuery));
        return ResponseEntity.ok(dtos) ;
    }

}
