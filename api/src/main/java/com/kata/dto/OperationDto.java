package com.kata.dto;

import com.kata.entity.OperationType;
import lombok.Builder;
import lombok.Data;
import java.time.LocalDateTime;

@Data
@Builder
public class OperationDto  {
    long id;
    LocalDateTime date;
    float amount;
    OperationType type;
    String accountNumber;
}
