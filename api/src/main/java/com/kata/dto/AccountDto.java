package com.kata.dto;

import com.kata.entity.AccountType;
import lombok.Builder;
import lombok.Data;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Builder
public class AccountDto {
    String accountNumber;
    BigDecimal balance;
    AccountType type;
    LocalDateTime creationDate;
}
