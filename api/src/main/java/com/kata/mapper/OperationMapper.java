package com.kata.mapper;

import com.kata.dto.OperationDto;
import com.kata.entity.Operation;
import org.mapstruct.Mapper;
import java.util.List;

@Mapper(componentModel = "spring")
public interface OperationMapper {
    OperationDto map(Operation operation);
    Operation map(OperationDto operationDto);
    default List<OperationDto>  mapToDtoList(List<Operation> consultHistory) {
        return consultHistory.stream().map(this::map).toList();
    }

}
