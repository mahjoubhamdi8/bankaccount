package com.kata.mapper;

import com.kata.command.account.CreateAccountAction;
import com.kata.dto.AccountDto;
import com.kata.entity.Account;
import org.mapstruct.Mapper;
import java.util.Set;


@Mapper(componentModel = "spring", uses = {OperationMapper.class})
public interface AccountMapper {
    AccountDto toDto(Account account);
    CreateAccountAction toCreateAction(AccountDto accountDto);
    Set<AccountDto> mapToDtoSet(Set<CreateAccountAction> accounts);
    Set<CreateAccountAction> mapToSet(Set<AccountDto> accountDtos);
}
