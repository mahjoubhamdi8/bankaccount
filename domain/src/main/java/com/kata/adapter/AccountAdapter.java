package com.kata.adapter;

import com.kata.command.account.*;
import com.kata.entity.Account;
import com.kata.entity.Operation;
import com.kata.ports.incoming.AccountServicePort;
import com.kata.mediator.Mediator;
import lombok.RequiredArgsConstructor;
import java.math.BigDecimal;
import java.util.List;

@RequiredArgsConstructor

public class AccountAdapter implements AccountServicePort {
    private final Mediator mediator;

    public Account withdrawAmount(WithdrawAction withdrawAction){
        return mediator.execute(withdrawAction);
    }

    public Account depositAmount(DepositAction depositAction){
        return mediator.execute(depositAction);
    }
    public Account createAccount (CreateAccountAction createAccountAction) {
        return mediator.execute(createAccountAction) ;
    }

    public Account getAccount (GetAccountQuery getAccountQuery) {
        return mediator.execute(getAccountQuery) ;
    }

    public BigDecimal getBalance (GetBalanceQuery getBalanceQuery) {
        return mediator.execute(getBalanceQuery) ;
    }

    public List<Operation> getHistory (GetHistoryAccountQuery getHistoryAccountQuery) {
        return mediator.execute(getHistoryAccountQuery) ;
    }
}
