package com.kata.command.account;

import com.kata.command.Command;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class GetAccountQuery implements Command {
    private String accountNumber ;
}
