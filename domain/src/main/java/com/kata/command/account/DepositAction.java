package com.kata.command.account;

import com.kata.command.Command;
import lombok.Builder;
import lombok.Value;
import java.math.BigDecimal;
@Value
@Builder
public class DepositAction implements Command {
    private String accountNumber;
    private BigDecimal amount;
}
