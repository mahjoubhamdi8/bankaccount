package com.kata.command.account;

import com.kata.command.Command;
import com.kata.entity.AccountType;
import lombok.Builder;
import lombok.Value;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.stream.Stream;
@Value
@Builder
public class CreateAccountAction implements Command {
    String accountNumber;
    BigDecimal balance;
    AccountType type;
    LocalDateTime creationDate;
    public boolean isValid() {
        return Stream.of(
                        accountNumber,
                        balance,
                        type,
                        creationDate).noneMatch(Objects::isNull) &&
                creationDate.isBefore(LocalDateTime.now()) ;
    }
}
