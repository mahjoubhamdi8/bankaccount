package com.kata.command.account;

import com.kata.command.Command;
import lombok.Builder;
import lombok.Value;
import java.time.LocalDateTime;
@Value
@Builder
public class GetHistoryAccountQuery implements Command {
    private String accountNumber ;
    private LocalDateTime sinceDate ;
}
