package com.kata.exception;

public class NullCommandException extends RuntimeException {
    public NullCommandException(String message) {
        super(message);
    }
}
