package com.kata.exception;

public class HandlerException extends RuntimeException{

    public HandlerException(String message) {
        super(message.toString());
    }

}
