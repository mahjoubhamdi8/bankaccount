package com.kata.entity;
import lombok.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
public class Account {
    String accountNumber;
    BigDecimal balance;
    AccountType type;
    LocalDateTime creationDate;


}
