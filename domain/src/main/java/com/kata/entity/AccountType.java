package com.kata.entity;

public enum AccountType {
    SAVINGS, CHECKING
}
