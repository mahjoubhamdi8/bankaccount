package com.kata.entity;

public enum OperationType {
    WITHDRAWAL, DEPOSIT
}
