package com.kata.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Operation {
    long id;
    LocalDateTime date;
    float amount;
    OperationType type;
    String accountNumber;
}
