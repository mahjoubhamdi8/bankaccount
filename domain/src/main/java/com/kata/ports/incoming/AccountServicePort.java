package com.kata.ports.incoming;

import com.kata.command.account.*;
import com.kata.entity.Account;
import com.kata.entity.Operation;

import java.math.BigDecimal;
import java.util.List;

public interface AccountServicePort {

    Account createAccount (CreateAccountAction createAccountAction) ;
    Account getAccount (GetAccountQuery getAccountQuery) ;

    Account withdrawAmount(WithdrawAction withdrawAction) ;

    Account depositAmount(DepositAction depositAction) ;

    BigDecimal getBalance (GetBalanceQuery getBalanceQuery);

    List<Operation> getHistory (GetHistoryAccountQuery getHistoryAccountQuery);



}
