package com.kata.ports.persistance;

import com.kata.exception.AccountNotFoundException;
import com.kata.entity.Account;

public interface AccountPersistencePort {
    Account createAccount(Account account);

    Account updateAccount(Account account);

    Account getAccountByNumber(String accountNumber) throws AccountNotFoundException;


}
