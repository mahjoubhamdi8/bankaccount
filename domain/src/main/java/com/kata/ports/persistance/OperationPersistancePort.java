package com.kata.ports.persistance;

import com.kata.entity.Operation;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public interface OperationPersistancePort {

    Operation createOperation (Operation operation) ;

    List<Operation> getOperationsByAccountNumberSinceDate(String accountNumber , LocalDateTime date) ;
}
