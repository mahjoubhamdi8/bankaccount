package com.kata.mediator;

import com.kata.command.Command;
import com.kata.entity.Account;

public interface Mediator {
    <R, T extends Command> R execute(T command) ;
}
