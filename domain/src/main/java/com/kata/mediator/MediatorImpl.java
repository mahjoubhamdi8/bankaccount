package com.kata.mediator;

import com.kata.command.*;
import com.kata.command.account.CreateAccountAction;
import com.kata.command.account.DepositAction;
import com.kata.command.account.GetAccountQuery;
import com.kata.command.account.WithdrawAction;
import com.kata.entity.Account;
import com.kata.exception.HandlerException;
import com.kata.exception.NullCommandException;
import com.kata.usecase.account.*;
import lombok.AllArgsConstructor;

import java.util.List;
import java.util.Objects;
import java.util.Set;

@AllArgsConstructor
public class MediatorImpl implements Mediator {
    private Set<UseCaseAccount<?,?>> usercases;
    @Override
    public <R, T extends Command> R execute(T command) {
        if (Objects.isNull(command)) {
            throw new NullCommandException("Command Null");
        }
        return usercases.stream().filter(candidate -> candidate.canExecute(command))
                .findFirst()
                .map(usecase -> ((UseCaseAccount<R ,T>) usecase).execute(command))
                .orElseThrow(() -> {
                            throw new HandlerException(command.getClass().getName() + " exception");
                        }
                );
    }
}