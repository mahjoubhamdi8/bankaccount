package com.kata.usecase.account;

import com.kata.command.Command;
import com.kata.command.account.GetAccountQuery;
import com.kata.entity.Account;
import com.kata.exception.AccountNotFoundException;
import com.kata.ports.persistance.AccountPersistencePort;
import lombok.AllArgsConstructor;

@AllArgsConstructor

public class GetAccountUseCase implements UseCaseAccount<Account ,GetAccountQuery> {
    private AccountPersistencePort accountPersistencePort;


    @Override
    public Account execute(GetAccountQuery getAccountQuery)  {
        try {
            return accountPersistencePort.getAccountByNumber(getAccountQuery.getAccountNumber());
        }
        catch (Exception ex) {
            throw new AccountNotFoundException("Account Not Found") ;
        }
    }

    @Override
    public <C extends Command> boolean canExecute(C command) {
        return GetAccountQuery.class
                .equals(command.getClass());
    }

}
