package com.kata.usecase.account;

import com.kata.command.Command;
import com.kata.command.account.GetHistoryAccountQuery;
import com.kata.entity.Operation;
import com.kata.ports.persistance.OperationPersistancePort;
import lombok.AllArgsConstructor;

import java.util.List;

@AllArgsConstructor
public class ConsultHistoryAccountUseCase implements UseCaseAccount<List<Operation>, GetHistoryAccountQuery> {
    private OperationPersistancePort operationPersistencePort;


    @Override
    public List<Operation> execute(GetHistoryAccountQuery historyAccountQuery)  {
        return operationPersistencePort.getOperationsByAccountNumberSinceDate(historyAccountQuery.getAccountNumber(), historyAccountQuery.getSinceDate());
    }

    @Override
    public <C extends Command> boolean canExecute(C command) {
        return GetHistoryAccountQuery.class
                .equals(command.getClass());
    }
}
