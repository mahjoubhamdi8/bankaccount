package com.kata.usecase.account;

import com.kata.command.Command;
import com.kata.command.account.WithdrawAction;
import com.kata.entity.Account;
import com.kata.entity.Operation;
import com.kata.entity.OperationType;
import com.kata.exception.InvalidInputException;
import com.kata.ports.persistance.AccountPersistencePort;

import com.kata.ports.persistance.OperationPersistancePort;
import lombok.AllArgsConstructor;

import java.time.LocalDateTime;

@AllArgsConstructor
public class WithdrawAmountUseCase implements UseCaseAccount<Account , WithdrawAction> {

    private AccountPersistencePort accountPersistencePort;
    private OperationPersistancePort operationPersistancePort;
    @Override
    public Account execute(WithdrawAction withdrawAction)  {
        Account account = accountPersistencePort.getAccountByNumber(withdrawAction.getAccountNumber());
        if (account.getBalance().compareTo(withdrawAction.getAmount()) < 0) {
            throw new InvalidInputException("Insufficient funds");
        }
        account.setBalance(account.getBalance().subtract(withdrawAction.getAmount()));
        operationPersistancePort.createOperation(Operation.builder().
                accountNumber(account.getAccountNumber()).
                amount(withdrawAction.getAmount().floatValue()).
                date(LocalDateTime.now()).
                type(OperationType.WITHDRAWAL).build());
        return accountPersistencePort.updateAccount(account);
    }

    @Override
    public <C extends Command> boolean canExecute(C command) {
        return WithdrawAction.class
                .equals(command.getClass());
    }
}
