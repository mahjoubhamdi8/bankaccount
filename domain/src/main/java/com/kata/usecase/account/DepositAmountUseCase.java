package com.kata.usecase.account;

import com.kata.command.Command;
import com.kata.command.account.DepositAction;
import com.kata.entity.Account;
import com.kata.entity.Operation;
import com.kata.entity.OperationType;
import com.kata.exception.InvalidInputException;
import com.kata.ports.persistance.AccountPersistencePort;

import com.kata.ports.persistance.OperationPersistancePort;
import lombok.AllArgsConstructor;

import java.time.LocalDateTime;

@AllArgsConstructor
public class DepositAmountUseCase implements UseCaseAccount<Account , DepositAction> {
    private AccountPersistencePort accountPersistencePort;
    private OperationPersistancePort operationPersistancePort;
    @Override
    public Account execute(DepositAction depositAction) {
        Account account = accountPersistencePort.getAccountByNumber(depositAction.getAccountNumber());
        if (depositAction.getAmount().floatValue()<0 ) {
            throw new InvalidInputException("Negative deposit amount") ;
        }
        account.setBalance(account.getBalance().add(depositAction.getAmount()));
        operationPersistancePort.createOperation(Operation.builder().
                accountNumber(account.getAccountNumber()).
                amount(depositAction.getAmount().floatValue()).
                date(LocalDateTime.now()).
                type(OperationType.DEPOSIT).build());
        return accountPersistencePort.updateAccount(account);
    }
    @Override
    public <C extends Command> boolean canExecute(C command) {
        return DepositAction.class
                .equals(command.getClass());
    }


}
