package com.kata.usecase.account;

import com.kata.command.Command;
import com.kata.command.account.CreateAccountAction;
import com.kata.entity.Account;
import com.kata.exception.InvalidInputException;
import com.kata.ports.persistance.AccountPersistencePort;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class CreateAccountUseCase implements UseCaseAccount<Account ,CreateAccountAction> {
    private final AccountPersistencePort accountPersistencePort;

    @Override
    public Account execute(CreateAccountAction createAccountAction) {
        if (!(createAccountAction.isValid()) ) {
            throw new InvalidInputException("Invalid Input Exception");
        }
        Account account = Account.builder().accountNumber(createAccountAction.getAccountNumber())
                .type(createAccountAction.getType())
                .balance(createAccountAction.getBalance())
                .creationDate(createAccountAction.getCreationDate()).build() ;
        return accountPersistencePort.createAccount(account);
    }
    @Override
    public <C extends Command> boolean canExecute(C command) {
        return CreateAccountAction.class
                .equals(command.getClass());
    }

}
