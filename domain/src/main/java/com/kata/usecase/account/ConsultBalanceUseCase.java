package com.kata.usecase.account;

import com.kata.command.Command;
import com.kata.command.account.GetBalanceQuery;
import com.kata.ports.persistance.AccountPersistencePort;
import lombok.AllArgsConstructor;

import java.math.BigDecimal;

@AllArgsConstructor
public class ConsultBalanceUseCase implements UseCaseAccount<BigDecimal, GetBalanceQuery> {
    private AccountPersistencePort accountPersistencePort;

    @Override
    public BigDecimal execute(GetBalanceQuery command)  {
        return accountPersistencePort.getAccountByNumber(command.getAccountNumber()).getBalance();
    }

    @Override
    public <C extends Command> boolean canExecute(C command) {
        return GetBalanceQuery.class
                .equals(command.getClass());
    }

}
