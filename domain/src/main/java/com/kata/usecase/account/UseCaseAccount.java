package com.kata.usecase.account;

import com.kata.command.Command;
import com.kata.command.account.WithdrawAction;
import com.kata.entity.Account;

public interface UseCaseAccount<R , T extends Command>{

    R  execute (T command) ;

    <C extends Command> boolean canExecute(C command) ;

}
