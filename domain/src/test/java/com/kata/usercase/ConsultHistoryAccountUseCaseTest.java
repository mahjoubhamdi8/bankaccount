package com.kata.usercase;

import com.kata.adapter.AccountAdapter;
import com.kata.command.account.GetHistoryAccountQuery;
import com.kata.entity.Operation;
import com.kata.entity.OperationType;
import com.kata.mediator.MediatorImpl;
import com.kata.ports.persistance.AccountPersistencePort;
import com.kata.ports.persistance.OperationPersistancePort;
import com.kata.usecase.account.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;

@ExtendWith(MockitoExtension.class)
public class ConsultHistoryAccountUseCaseTest {
    private static final BigDecimal ACCOUNT_BALANCE = new BigDecimal("200.587");
    @InjectMocks
    ConsultHistoryAccountUseCase consultHistoryAccountUseCase;
    @Mock
    AccountPersistencePort accountPersistencePort;
    @Mock
    OperationPersistancePort operationPersistancePort;
    MediatorImpl mediator ;
    AccountAdapter accountAdapter ;
    List<Operation> operations ;

    @BeforeEach
    void setup() {
        CreateAccountUseCase createAccountUseCase = new CreateAccountUseCase(accountPersistencePort) ;
        DepositAmountUseCase depositAmountUseCase = new DepositAmountUseCase(accountPersistencePort ,operationPersistancePort) ;
        ConsultBalanceUseCase consultBalanceUseCase = new ConsultBalanceUseCase(accountPersistencePort) ;
        GetAccountUseCase getAccountUseCase1 = new GetAccountUseCase(accountPersistencePort) ;
        WithdrawAmountUseCase withdrawAmountUseCase = new WithdrawAmountUseCase(accountPersistencePort, operationPersistancePort) ;
        ConsultHistoryAccountUseCase consultHistoryAccountUseCase1 = new ConsultHistoryAccountUseCase(operationPersistancePort);
        Set<UseCaseAccount<?, ?>> usercases =  Set.of(consultHistoryAccountUseCase1,getAccountUseCase1 ,createAccountUseCase ,depositAmountUseCase ,consultBalanceUseCase , withdrawAmountUseCase);
        mediator = new MediatorImpl(usercases)  ;
        accountAdapter = new AccountAdapter(mediator);

        operations = List.of(Operation.builder().id(1).amount(10).date(LocalDateTime.now()).accountNumber("acc1").type(OperationType.WITHDRAWAL).build(),
                Operation.builder().id(2).amount(10).date(LocalDateTime.now()).accountNumber("acc1").type(OperationType.WITHDRAWAL).build(),
                Operation.builder().id(3).amount(10).date(LocalDateTime.now()).accountNumber("acc1").type(OperationType.WITHDRAWAL).build(),
                Operation.builder().id(4).amount(10).date(LocalDateTime.now()).accountNumber("acc1").type(OperationType.WITHDRAWAL).build()
        ) ;

    }

    @Test
    void testConsultHistory() {
        Mockito.when(operationPersistancePort.getOperationsByAccountNumberSinceDate(anyString(), any())).thenReturn(operations);
        GetHistoryAccountQuery getHistoryAccountQuery = GetHistoryAccountQuery.builder().accountNumber("acc1").sinceDate(LocalDateTime.now()).build();
        assertEquals(operations, consultHistoryAccountUseCase.execute(getHistoryAccountQuery));
    }
    @Test
    void testConsultHistoryWithMediator() {
        Mockito.when(operationPersistancePort.getOperationsByAccountNumberSinceDate(anyString(), any())).thenReturn(operations);
        GetHistoryAccountQuery getHistoryAccountQuery = GetHistoryAccountQuery.builder().accountNumber("acc1").sinceDate(LocalDateTime.now()).build();
        assertEquals(operations, mediator.execute(getHistoryAccountQuery));
    }
    @Test
    void testConsultHistoryWithAdapter() {
        Mockito.when(operationPersistancePort.getOperationsByAccountNumberSinceDate(anyString(), any())).thenReturn(operations);
        GetHistoryAccountQuery getHistoryAccountQuery = GetHistoryAccountQuery.builder().accountNumber("acc1").sinceDate(LocalDateTime.now()).build();
        assertEquals(operations, accountAdapter.getHistory(getHistoryAccountQuery));
    }
}
