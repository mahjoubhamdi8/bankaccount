package com.kata.usercase;

import com.kata.adapter.AccountAdapter;
import com.kata.command.account.DepositAction;
import com.kata.entity.Operation;
import com.kata.entity.OperationType;
import com.kata.exception.InvalidInputException;
import com.kata.mediator.MediatorImpl;
import com.kata.entity.Account;
import com.kata.ports.persistance.AccountPersistencePort;
import com.kata.ports.persistance.OperationPersistancePort;
import com.kata.usecase.account.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Set;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class DepositAmountUseCaseTest {
    @InjectMocks
    DepositAmountUseCase depositAmountUseCase;
    AccountAdapter accountAdapter ;
    MediatorImpl mediator ;
    @Mock
    AccountPersistencePort accountPersistencePort;
    @Mock
    OperationPersistancePort operationPersistancePort;
    Account account;
    Operation operation;
    float amount ;
    @BeforeEach
    void setup() {
        this.account = Account.builder().balance(new BigDecimal("200.587")).build();
        this.amount = 100 ;
        this.operation = Operation.builder().id(1).date(LocalDateTime.now()).type(OperationType.DEPOSIT).amount(amount).accountNumber("acc1").build();
        CreateAccountUseCase createAccountUseCase = new CreateAccountUseCase(accountPersistencePort) ;
        DepositAmountUseCase depositAmountUseCase = new DepositAmountUseCase(accountPersistencePort ,operationPersistancePort) ;
        ConsultBalanceUseCase consultBalanceUseCase = new ConsultBalanceUseCase(accountPersistencePort) ;
        GetAccountUseCase getAccountUseCase = new GetAccountUseCase(accountPersistencePort) ;
        WithdrawAmountUseCase withdrawAmountUseCase = new WithdrawAmountUseCase(accountPersistencePort, operationPersistancePort) ;
        ConsultHistoryAccountUseCase consultHistoryAccountUseCase = new ConsultHistoryAccountUseCase(operationPersistancePort);

        Set<UseCaseAccount<?, ?>> usercases =  Set.of(consultHistoryAccountUseCase,getAccountUseCase ,createAccountUseCase ,depositAmountUseCase ,consultBalanceUseCase , withdrawAmountUseCase);
        mediator = new MediatorImpl(usercases)  ;
        accountAdapter = new AccountAdapter(mediator);
    }

    @Test
    void ShouldDepositAmountWithValidAction() {
        Mockito.when(accountPersistencePort.getAccountByNumber(anyString())).thenReturn(this.account);
        when(accountPersistencePort.updateAccount(any(Account.class)))
                .thenAnswer(invocation -> invocation.getArgument(0));
        Mockito.when(operationPersistancePort.createOperation(any(Operation.class))).thenReturn(this.operation);
        DepositAction depositAction = DepositAction.builder().amount(new BigDecimal(amount)).accountNumber("ACC1").build() ;
        depositAmountUseCase.execute(depositAction);
        assertEquals(new BigDecimal("300.587"), account.getBalance());
    }

    @Test
    void ShouldThrowsExceptionWhenDepositNegativeAmount() {
        Mockito.when(accountPersistencePort.getAccountByNumber(anyString())).thenReturn(this.account);
        DepositAction depositAction = DepositAction.builder().amount(new BigDecimal("-300")).accountNumber("ACC1").build();
        assertThrows(InvalidInputException.class, () -> depositAmountUseCase.execute(depositAction));
    }

    @Test
    void ShouldDepositAmountWithValidActionUsingMediator() {
        Mockito.when(accountPersistencePort.getAccountByNumber(anyString())).thenReturn(this.account);
        Mockito.when(operationPersistancePort.createOperation(any(Operation.class))).thenReturn(this.operation);
        when(accountPersistencePort.updateAccount(any(Account.class)))
                .thenAnswer(invocation -> invocation.getArgument(0));
        DepositAction depositAction = DepositAction.builder().amount(new BigDecimal(amount)).accountNumber("ACC1").build() ;
        mediator.execute(depositAction);
        assertEquals(new BigDecimal("300.587"), account.getBalance());
    }

    @Test
    void ShouldThrowsExceptionWhenDepositNegativeAmountUsingMediator() {
        Mockito.when(accountPersistencePort.getAccountByNumber(anyString())).thenReturn(this.account);
        DepositAction depositAction = DepositAction.builder().amount(new BigDecimal("-300")).accountNumber("ACC1").build() ;
        assertThrows(InvalidInputException.class, () -> mediator.execute(depositAction));
    }

    @Test
    void ShouldThrowsExceptionWhenDepositNegativeAmountUsingAdapter() {
        Mockito.when(accountPersistencePort.getAccountByNumber(anyString())).thenReturn(this.account);
        DepositAction depositAction = DepositAction.builder().amount(new BigDecimal("-300")).accountNumber("ACC1").build() ;
        assertThrows(InvalidInputException.class, () -> accountAdapter.depositAmount(depositAction));
    }
    @Test
    void ShouldDepositAmountWithValidActionUsingAdapter() {
        Mockito.when(accountPersistencePort.getAccountByNumber(anyString())).thenReturn(this.account);
        when(accountPersistencePort.updateAccount(any(Account.class)))
                .thenAnswer(invocation -> invocation.getArgument(0));
        Mockito.when(operationPersistancePort.createOperation(any(Operation.class))).thenReturn(this.operation);
        DepositAction depositAction = DepositAction.builder().amount(new BigDecimal(amount)).accountNumber("ACC1").build() ;
        accountAdapter.depositAmount(depositAction);
        assertEquals(new BigDecimal("300.587"), account.getBalance());
    }

}
