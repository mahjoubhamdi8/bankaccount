package com.kata.usercase;
import com.kata.adapter.AccountAdapter;
import com.kata.command.account.GetAccountQuery;
import com.kata.exception.AccountNotFoundException;
import com.kata.mediator.MediatorImpl;
import com.kata.ports.persistance.AccountPersistencePort;
import com.kata.entity.Account;
import com.kata.entity.AccountType;
import com.kata.ports.persistance.OperationPersistancePort;
import com.kata.usecase.account.*;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
@ExtendWith(MockitoExtension.class)
public class GetAccountUseCaseTest {
    private static final BigDecimal ACCOUNT_BALANCE = new BigDecimal("200.587");
    @InjectMocks
    GetAccountUseCase getAccountUseCase;
    @Mock
    AccountPersistencePort accountPersistencePort;
    @Mock
    OperationPersistancePort operationPersistancePort;
    MediatorImpl mediator ;
    AccountAdapter accountAdapter ;

    @BeforeEach
    void setup() {
        CreateAccountUseCase createAccountUseCase = new CreateAccountUseCase(accountPersistencePort) ;
        DepositAmountUseCase depositAmountUseCase = new DepositAmountUseCase(accountPersistencePort ,operationPersistancePort) ;
        ConsultBalanceUseCase consultBalanceUseCase = new ConsultBalanceUseCase(accountPersistencePort) ;
        GetAccountUseCase getAccountUseCase1 = new GetAccountUseCase(accountPersistencePort) ;
        ConsultHistoryAccountUseCase consultHistoryAccountUseCase = new ConsultHistoryAccountUseCase(operationPersistancePort);
        WithdrawAmountUseCase withdrawAmountUseCase = new WithdrawAmountUseCase(accountPersistencePort, operationPersistancePort) ;
        Set<UseCaseAccount<?, ?>> usercases =  Set.of(consultHistoryAccountUseCase,getAccountUseCase1 ,createAccountUseCase ,depositAmountUseCase ,consultBalanceUseCase , withdrawAmountUseCase);
        mediator = new MediatorImpl(usercases)  ;
        accountAdapter = new AccountAdapter(mediator);
    }

    @Test
    void shouldGetAccountByAccountNumber() {
        Mockito.when(accountPersistencePort.getAccountByNumber(anyString())).thenReturn(getTestAccount());
        GetAccountQuery getAccountQuery = GetAccountQuery.builder().accountNumber("acc1").build();
        Assertions.assertThat(Account.builder().accountNumber("acc1").balance(ACCOUNT_BALANCE).type(AccountType.SAVINGS).creationDate(LocalDateTime.of(2023, 7, 9, 10, 30, 0)).build()).isEqualTo( getAccountUseCase.execute(getAccountQuery));
    }

    @Test
    void shouldGetAccountByAccountNumberWithMediator() {
        Mockito.when(accountPersistencePort.getAccountByNumber(anyString())).thenReturn(getTestAccount());
        GetAccountQuery getAccountQuery = GetAccountQuery.builder().accountNumber("acc1").build();
        Assertions.assertThat(Account.builder().accountNumber("acc1").balance(ACCOUNT_BALANCE).type(AccountType.SAVINGS).creationDate(LocalDateTime.of(2023, 7, 9, 10, 30, 0)).build()).isEqualTo( mediator.execute(getAccountQuery));
    }
    @Test
    void shouldGetAccountByAccountNumberWithAdapter() {
        Mockito.when(accountPersistencePort.getAccountByNumber(anyString())).thenReturn(getTestAccount());
        GetAccountQuery getAccountQuery = GetAccountQuery.builder().accountNumber("acc1").build();
        Assertions.assertThat(Account.builder().accountNumber("acc1").balance(ACCOUNT_BALANCE).type(AccountType.SAVINGS).creationDate(LocalDateTime.of(2023, 7, 9, 10, 30, 0)).build()).isEqualTo( accountAdapter.getAccount(getAccountQuery));
    }


    private Account getTestAccount() {
        return Account.builder().accountNumber("acc1").balance(ACCOUNT_BALANCE).type(AccountType.SAVINGS).creationDate(LocalDateTime.of(2023, 7, 9, 10, 30, 0)).build();
    }

}
