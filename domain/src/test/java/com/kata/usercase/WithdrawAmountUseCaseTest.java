package com.kata.usercase;

import com.kata.adapter.AccountAdapter;
import com.kata.command.account.WithdrawAction;
import com.kata.entity.Operation;
import com.kata.entity.OperationType;
import com.kata.exception.InvalidInputException;
import com.kata.entity.Account;
import com.kata.mediator.MediatorImpl;
import com.kata.ports.persistance.AccountPersistencePort;
import com.kata.ports.persistance.OperationPersistancePort;
import com.kata.usecase.account.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.internal.matchers.Null;
import org.mockito.junit.jupiter.MockitoExtension;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class WithdrawAmountUseCaseTest {
    @InjectMocks
    WithdrawAmountUseCase withdrawAmountUseCase;
    AccountAdapter accountAdapter ;
    MediatorImpl mediator ;
    @Mock
    AccountPersistencePort accountPersistencePort;
    @Mock
    OperationPersistancePort operationPersistancePort;
    Account account;
    Operation operation;
    float amount ;
    @BeforeEach
    void setup() {
        this.account = Account.builder().balance(new BigDecimal("200.587")).build();
        this.amount = 100 ;
        this.operation = Operation.builder().id(1).date(LocalDateTime.now()).type(OperationType.WITHDRAWAL).amount(amount).accountNumber("acc1").build();
        CreateAccountUseCase createAccountUseCase = new CreateAccountUseCase(accountPersistencePort) ;
        WithdrawAmountUseCase withdrawAmountUseCase = new WithdrawAmountUseCase(accountPersistencePort ,operationPersistancePort) ;
        ConsultBalanceUseCase consultBalanceUseCase = new ConsultBalanceUseCase(accountPersistencePort) ;
        GetAccountUseCase getAccountUseCase = new GetAccountUseCase(accountPersistencePort) ;
        ConsultHistoryAccountUseCase consultHistoryAccountUseCase = new ConsultHistoryAccountUseCase(operationPersistancePort);
        DepositAmountUseCase depositAmountUseCase = new DepositAmountUseCase(accountPersistencePort, operationPersistancePort) ;
        Set<UseCaseAccount<?, ?>> usercases =  Set.of(consultHistoryAccountUseCase,getAccountUseCase ,createAccountUseCase ,depositAmountUseCase ,consultBalanceUseCase , withdrawAmountUseCase);
        mediator = new MediatorImpl(usercases)  ;
        accountAdapter = new AccountAdapter(mediator);
    }

    @Test
    void ShouldWithdrawAmountWithValidAction() {
        Mockito.when(accountPersistencePort.getAccountByNumber(anyString())).thenReturn(this.account);
        when(accountPersistencePort.updateAccount(any(Account.class)))
                .thenAnswer(invocation -> invocation.getArgument(0));
        Mockito.when(operationPersistancePort.createOperation(any(Operation.class))).thenReturn(this.operation);
        WithdrawAction withdrawAction = WithdrawAction.builder().amount(new BigDecimal(amount)).accountNumber("ACC1").build() ;
        withdrawAmountUseCase.execute(withdrawAction);
        assertEquals(new BigDecimal("100.587"), account.getBalance());
    }

    @Test
    void ShouldThrowsExceptionWhenWithdrawNegativeAmount() {
        Mockito.when(accountPersistencePort.getAccountByNumber(anyString())).thenReturn(this.account);
        WithdrawAction withdrawAction = WithdrawAction.builder().amount(new BigDecimal("300")).accountNumber("ACC1").build();
        assertThrows(InvalidInputException.class, () -> withdrawAmountUseCase.execute(withdrawAction));
    }

    @Test
    void ShouldWithdrawAmountWithValidActionUsingMediator() {
        Mockito.when(accountPersistencePort.getAccountByNumber(anyString())).thenReturn(this.account);
        Mockito.when(operationPersistancePort.createOperation(any(Operation.class))).thenReturn(this.operation);
        when(accountPersistencePort.updateAccount(any(Account.class)))
                .thenAnswer(invocation -> invocation.getArgument(0));
        WithdrawAction withdrawAction = WithdrawAction.builder().amount(new BigDecimal(amount)).accountNumber("ACC1").build() ;
        mediator.execute(withdrawAction);
        assertEquals(new BigDecimal("100.587"), account.getBalance());
    }

    @Test
    void ShouldThrowsExceptionWhenWithdrawNegativeAmountUsingMediator() {
        Mockito.when(accountPersistencePort.getAccountByNumber(anyString())).thenReturn(this.account);
        WithdrawAction withdrawAction = WithdrawAction.builder().amount(new BigDecimal("300")).accountNumber("ACC1").build() ;
        assertThrows(InvalidInputException.class, () -> mediator.execute(withdrawAction));
    }

    @Test
    void ShouldThrowsExceptionWhenWithdrawNegativeAmountUsingAdapter() {
        Mockito.when(accountPersistencePort.getAccountByNumber(anyString())).thenReturn(this.account);
        WithdrawAction withdrawAction = WithdrawAction.builder().amount(new BigDecimal("300")).accountNumber("ACC1").build() ;
        assertThrows(InvalidInputException.class, () -> accountAdapter.withdrawAmount(withdrawAction));
    }
    @Test
    void ShouldWithdrawAmountWithValidActionUsingAdapter() {
        Mockito.when(accountPersistencePort.getAccountByNumber(anyString())).thenReturn(this.account);
        when(accountPersistencePort.updateAccount(any(Account.class)))
                .thenAnswer(invocation -> invocation.getArgument(0));
        Mockito.when(operationPersistancePort.createOperation(any(Operation.class))).thenReturn(this.operation);
        WithdrawAction withdrawAction = WithdrawAction.builder().amount(new BigDecimal(amount)).accountNumber("ACC1").build() ;
        accountAdapter.withdrawAmount(withdrawAction);
        assertEquals(new BigDecimal("100.587"), account.getBalance());
    }

}
