package com.kata.usercase;

import com.kata.adapter.AccountAdapter;
import com.kata.command.account.CreateAccountAction;
import com.kata.entity.Account;
import com.kata.entity.AccountType;
import com.kata.exception.HandlerException;
import com.kata.exception.InvalidInputException;
import com.kata.exception.NullCommandException;
import com.kata.mediator.MediatorImpl;
import com.kata.ports.persistance.AccountPersistencePort;
import com.kata.ports.persistance.OperationPersistancePort;
import com.kata.usecase.account.*;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.assertj.core.api.Assertions;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.stream.Stream;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
@ExtendWith(MockitoExtension.class)
public class CreateAccountUseCaseTest {
    private static final BigDecimal ACCOUNT_BALANCE = new BigDecimal("200.587");
    @InjectMocks
    CreateAccountUseCase createAccountUseCase;
    @Mock
    AccountPersistencePort accountPersistencePort;
    @Mock
    OperationPersistancePort operationPersistancePort;
    MediatorImpl mediator ;
    AccountAdapter accountAdapter ;

    @BeforeEach
    void setup() {
        CreateAccountUseCase createAccountUseCase = new CreateAccountUseCase(accountPersistencePort) ;
        DepositAmountUseCase depositAmountUseCase = new DepositAmountUseCase(accountPersistencePort ,operationPersistancePort) ;
        ConsultBalanceUseCase consultBalanceUseCase = new ConsultBalanceUseCase(accountPersistencePort) ;
        GetAccountUseCase getAccountUseCase = new GetAccountUseCase(accountPersistencePort) ;
        WithdrawAmountUseCase withdrawAmountUseCase = new WithdrawAmountUseCase(accountPersistencePort, operationPersistancePort) ;
        ConsultHistoryAccountUseCase consultHistoryAccountUseCase = new ConsultHistoryAccountUseCase(operationPersistancePort);
        Set<UseCaseAccount<?, ?>> usercases =  Set.of(consultHistoryAccountUseCase ,getAccountUseCase ,createAccountUseCase ,depositAmountUseCase ,consultBalanceUseCase , withdrawAmountUseCase);
        mediator = new MediatorImpl(usercases)  ;
        accountAdapter = new AccountAdapter(mediator);
    }

    @Test
    void shouldCreateAccountWithMediator() {
        // Given
        Mockito.when(accountPersistencePort.createAccount(any(Account.class)))
                .thenAnswer(invocation -> invocation.getArgument(0));
        com.kata.command.account.CreateAccountAction action = com.kata.command.account.CreateAccountAction.builder()
                .accountNumber(RandomStringUtils.randomAlphabetic(25))
                .balance(BigDecimal.valueOf(2500))
                .creationDate(LocalDateTime.of(2023, 7, 9, 10, 30, 0))
                .type(AccountType.SAVINGS)
                .build();

        Account expected = Account.builder()
                .accountNumber(action.getAccountNumber())
                .balance(action.getBalance())
                .creationDate(action.getCreationDate())
                .type(AccountType.SAVINGS)
                .build();

        // WHEN
        Account actual = mediator.execute(action);

        // Then
        Assertions.assertThat(actual).isEqualTo(expected);
    }

    @Test
    void shouldCreateAccountWithAdapter() {
        // Given
        Mockito.when(accountPersistencePort.createAccount(any(Account.class)))
                .thenAnswer(invocation -> invocation.getArgument(0));
        com.kata.command.account.CreateAccountAction action = com.kata.command.account.CreateAccountAction.builder()
                .accountNumber(RandomStringUtils.randomAlphabetic(25))
                .balance(BigDecimal.valueOf(2500))
                .creationDate(LocalDateTime.of(2023, 7, 9, 10, 30, 0))
                .type(AccountType.SAVINGS)
                .build();

        Account expected = Account.builder()
                .accountNumber(action.getAccountNumber())
                .balance(action.getBalance())
                .creationDate(action.getCreationDate())
                .type(AccountType.SAVINGS)
                .build();
        // WHEN
        Account actual = accountAdapter.createAccount(action) ;

        // Then
        Assertions.assertThat(actual).isEqualTo(expected);
    }

    public static Stream<CreateAccountAction> produceInvalidCreateAction() {
        return Stream.of(

                com.kata.command.account.CreateAccountAction.builder().build(),
                com.kata.command.account.CreateAccountAction.builder()
                        .balance(ACCOUNT_BALANCE)
                        .type(AccountType.SAVINGS)
                        .build(),
                com.kata.command.account.CreateAccountAction.builder()
                        .accountNumber("acc1")
                        .balance(ACCOUNT_BALANCE)
                        .build(),
                com.kata.command.account.CreateAccountAction.builder()
                        .accountNumber("acc1")
                        .balance(ACCOUNT_BALANCE)
                        .type(AccountType.SAVINGS)
                        .build(),
                com.kata.command.account.CreateAccountAction.builder()
                        .accountNumber("acc1")
                        .balance(ACCOUNT_BALANCE)
                        .type(AccountType.SAVINGS)
                        .build(),
                com.kata.command.account.CreateAccountAction.builder()
                        .accountNumber("acc1")
                        .balance(new BigDecimal("-200.587"))
                        .type(AccountType.SAVINGS)
                        .build()
        );
    }

    @ParameterizedTest
    @MethodSource("produceInvalidCreateAction")
    void shouldThrowInvalidExceptionWhenCalledWithInvalidCreateAction(CreateAccountAction action) {
        assertThatThrownBy(() -> accountAdapter.createAccount(action))
                .isInstanceOfAny(InvalidInputException.class , NullCommandException.class , HandlerException.class) ;
    }


}
