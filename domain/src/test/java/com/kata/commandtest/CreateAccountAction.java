package com.kata.commandtest;
import com.kata.entity.AccountType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.junit.jupiter.MockitoExtension;
import java.math.BigDecimal;
import java.util.stream.Stream;

@ExtendWith(MockitoExtension.class)
public class CreateAccountAction {

    private static final BigDecimal ACCOUNT_BALANCE = new BigDecimal("200.587");

    public static Stream<com.kata.command.account.CreateAccountAction> produceInvalidCreateAction() {
        return Stream.of(

                com.kata.command.account.CreateAccountAction.builder().build(),
                com.kata.command.account.CreateAccountAction.builder()
                        .balance(ACCOUNT_BALANCE)
                        .type(AccountType.SAVINGS)
                        .build(),
                com.kata.command.account.CreateAccountAction.builder()
                        .accountNumber("acc1")
                        .balance(ACCOUNT_BALANCE)
                        .build(),
                com.kata.command.account.CreateAccountAction.builder()
                        .accountNumber("acc1")
                        .balance(ACCOUNT_BALANCE)
                        .type(AccountType.SAVINGS)
                        .build(),
                com.kata.command.account.CreateAccountAction.builder()
                        .accountNumber("acc1")
                        .balance(ACCOUNT_BALANCE)
                        .type(AccountType.SAVINGS)
                        .build(),
                com.kata.command.account.CreateAccountAction.builder()
                        .accountNumber("acc1")
                        .balance(new BigDecimal("-200.587"))
                        .type(AccountType.SAVINGS)
                        .build()
        );
    }

    @ParameterizedTest
    @MethodSource("produceInvalidCreateAction")
    void shouldReturnFalseWhenInvalidAcount(com.kata.command.account.CreateAccountAction action) {
        Assertions.assertFalse(action.isValid()) ;
    }
}
