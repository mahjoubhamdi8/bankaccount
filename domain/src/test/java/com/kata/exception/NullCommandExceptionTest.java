package com.kata.exception;

import com.kata.command.account.WithdrawAction;
import com.kata.mediator.MediatorImpl;
import com.kata.ports.persistance.AccountPersistencePort;
import com.kata.ports.persistance.OperationPersistancePort;
import com.kata.usecase.account.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class NullCommandExceptionTest {
    MediatorImpl mediator ;
    AccountPersistencePort accountPersistencePort;
    OperationPersistancePort operationPersistancePort;
    @BeforeEach
    void setup() {
        CreateAccountUseCase createAccountUseCase = new CreateAccountUseCase(accountPersistencePort) ;
        WithdrawAmountUseCase withdrawAmountUseCase = new WithdrawAmountUseCase(accountPersistencePort ,operationPersistancePort) ;
        ConsultBalanceUseCase consultBalanceUseCase = new ConsultBalanceUseCase(accountPersistencePort) ;
        GetAccountUseCase getAccountUseCase = new GetAccountUseCase(accountPersistencePort) ;
        ConsultHistoryAccountUseCase consultHistoryAccountUseCase = new ConsultHistoryAccountUseCase(operationPersistancePort);
        DepositAmountUseCase depositAmountUseCase = new DepositAmountUseCase(accountPersistencePort, operationPersistancePort) ;
        Set<UseCaseAccount<?, ?>> usercases =  Set.of(withdrawAmountUseCase ,consultHistoryAccountUseCase,getAccountUseCase ,createAccountUseCase ,depositAmountUseCase ,consultBalanceUseCase);
        mediator = new MediatorImpl(usercases)  ;
    }
    @Test
    void ShouldWThrowNullCommandExeptionUsingAdapter() {
        WithdrawAction withdrawAction = WithdrawAction.builder().amount(null).accountNumber("ACC1").build() ;
        assertThrows(NullCommandException.class, () -> mediator.execute(null));
    }
}
